package me.pogorzelski.ekopt;

public class Rune implements Comparable<Rune> {

	private String description;
	private Boolean mustBePicked;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public int compareTo(Rune o) {
		// TODO Auto-generated method stub
		return this.description.compareTo(o.description);
	}

	@Override
	public int hashCode() {
		int result = description.hashCode();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null || getClass() != obj.getClass())
			return false;

		Rune rune = (Rune) obj;

		if (description.equals(rune.description))
			return true;

		return false;

	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return description;
	}

	public Boolean getMustBePicked() {
		return mustBePicked;
	}

	public void setMustBePicked(Boolean mustBePicked) {
		this.mustBePicked = mustBePicked;
	}
	
	
}
