package me.pogorzelski.ekopt;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;
import java.util.TreeMap;

import javafx.scene.control.TextArea;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Simulation {

	public static final Logger logger = LogManager.getLogger(Logger.class.getName());

	List<Card> cardsInit = new ArrayList<Card>();
	List<Rune> runesInit = new ArrayList<Rune>();
	List<Card> cardsAvailable = new ArrayList<Card>();
	List<Rune> runesAvailable = new ArrayList<Rune>();
	List<Deck> decksProcessed = new ArrayList<Deck>();
	Deck currentDeck = new Deck();
	Deck bestDeck = new Deck();
	Double topMpm = -100.0;
	Integer maxCost;
	public String target;
	String targetValue;
	String playerLvl;
	String innerIterations;
	String javaPath;
	Boolean checkCost;
    TextArea textAreaLog;

	public Simulation(String playerLvl) {
		Integer plLvl = Integer.valueOf(playerLvl);
		if (plLvl < 20) {
			this.maxCost = plLvl * 3 + 10;
		} else if (plLvl < 50) {
			this.maxCost = plLvl * 2 + 30;
		} else {
			this.maxCost = plLvl + 80;
		}
		// logger.info(plLvl);
		// logger.info(this.maxCost);
	}

	public List<Card> getCardsInit() {
		return cardsInit;
	}

	public void setCardsInit(List<Card> cardsInit) {
		this.cardsInit = cardsInit;
	}

	public List<Rune> getRunesInit() {
		return runesInit;
	}

	public void setRunesInit(List<Rune> runesInit) {
		this.runesInit = runesInit;
	}

	public List<Card> getCardsAvailable() {
		return cardsAvailable;
	}

	public void setCardsAvailable(List<Card> cardsAvailable) {
		this.cardsAvailable = cardsAvailable;
	}

	public List<Rune> getRunesAvailable() {
		return runesAvailable;
	}

	public void setRunesAvailable(List<Rune> runesAvailable) {
		this.runesAvailable = runesAvailable;
	}

	public void checkCost(Card card) throws IOException {
		File cardDataFile = new File(new File(App.dataDir, App.binDir), "supported.cards.txt");
		BufferedReader reader = new BufferedReader(new FileReader(cardDataFile));
		String line;
		while ((line = reader.readLine()) != null) {
			String cardToSearch = card.getDescription();
			if (cardToSearch.startsWith("LG:")) {
				cardToSearch = card.getDescription().replace("LG:", "");
			}
			if (cardToSearch.startsWith("E:")) {
				cardToSearch = card.getDescription().replace("E:", "");
			}
			if (cardToSearch.startsWith("DI:")) {
				cardToSearch = card.getDescription().replace("DI:", "");
			}
			if (cardToSearch.split(",")[0].trim().equals(line.split(",")[0].trim())) {
				Integer tmpCost = Integer.valueOf(line.split(",")[1].trim());
				String cardLvl = card.getDescription().split(",")[1].trim();
				Integer cardLvlFinal = 10;
				if (cardLvl.contains("+")) {
					cardLvlFinal = 10 + Integer.valueOf(cardLvl.split("\\+")[1]);
				} else {
					cardLvlFinal = Integer.valueOf(cardLvl);
				}
				Double additionalCost = Math.ceil((cardLvlFinal.doubleValue() - 10.0) / 2.0);
				logger.info("card : " + card.getDescription() + " lvl : " + cardLvl.toString() + " additional cost " + additionalCost.toString());
				card.setCost(tmpCost + additionalCost.intValue());
			}
		}
	}

	public void setCardsOrder(Deck deck) {
		// logger.info("Starting deck order");
		if (decksProcessed.size() == 0)
			return;
		Integer decksToConsider = Integer.min(10, decksProcessed.size());
		List<Deck> processedDecks = new ArrayList<Deck>(decksProcessed);
		Collections.sort((List<Deck>) processedDecks, Collections.reverseOrder());
		Map<Integer, Card> cardOrder = new TreeMap<Integer, Card>();
		List<Card> cardNotFound = new ArrayList<Card>();
		for (Card card : deck.cards) {
			Integer numberOfDecksIn = 0;
			Integer positionSum = 0;
			for (int i = 0; i < decksToConsider; i++) {
				Deck analyzedDeck = processedDecks.get(i);
				if (analyzedDeck.cards.contains(card)) {
					numberOfDecksIn++;
					positionSum += analyzedDeck.cards.indexOf(card);
				}
			}
			if (numberOfDecksIn > 0) {
				Integer bestPosition = Integer.valueOf(positionSum / numberOfDecksIn);
				Boolean placeFound = false;
				while (!placeFound) {
					if (cardOrder.containsKey(bestPosition)) {
						bestPosition++;
					} else {
						placeFound = true;
					}
				}
				// logger.info("Card " + card.getDescription()
				// + " best on position " + bestPosition);
				cardOrder.put(bestPosition, card);
			} else {
				cardNotFound.add(card);
			}
		}

		if (cardOrder.size() + cardNotFound.size() > 0) {
			deck.cards.clear();
			for (Entry<Integer, Card> card : cardOrder.entrySet()) {
				deck.cards.add(card.getValue());
			}
			for (Card card : cardNotFound) {
				deck.cards.add(card);
			}
		}

		// logger.info("Deck order finished");
	}

	public void loadCardSet(String cardSetData) throws IOException {
		logger.info("loading cards");
		BufferedReader reader = new BufferedReader(new StringReader(cardSetData));
		String line;
		Integer lineNumber = 0;
		while ((line = reader.readLine()) != null) {
			if (!line.startsWith("#")) {
				if (line.contains(",")) {
					Card card = new Card();
					if (line.startsWith("!")) {
						card.setMustBePicked(true);
						card.setDescription(line.trim().substring(1));
					} else {
						card.setMustBePicked(false);
						card.setDescription(line.trim());
					}
					if (line.split(",").length > 3) {
						card.setCost(Integer.valueOf(line.split(",")[1].trim()));
					} else {
						checkCost(card);
					}
					card.setCardId(lineNumber++);
					logger.info("Adding card " + card.getDescription() + " with ID " + card.getCardId().toString());
					cardsInit.add(card);
				} else if (!line.contains(",") && line.trim().length() > 0 && cardsInit.size() > 0) {
					Rune rune = new Rune();
					if (line.startsWith("!")) {
						rune.setMustBePicked(true);
						rune.setDescription(line.trim().substring(1));
					} else {
						rune.setMustBePicked(false);
						rune.setDescription(line.trim());
					}
					logger.info("Adding rune " + rune.getDescription());
					runesInit.add(rune);
				}
			}
		}
		// logger.info(cardsInit);
		// logger.info(runesInit);
		reader.close();
	}

	public void reset() {
		cardsAvailable.clear();
		cardsAvailable.addAll(cardsInit);
		runesAvailable.clear();
		runesAvailable.addAll(runesInit);
	}

	public void pickMustCards(Deck deck) {
		List<Card> cards = new ArrayList<Card>(this.cardsAvailable);
		List<Rune> runes = new ArrayList<Rune>(this.runesAvailable);

		for (Card card : cards) {
			if (card.getMustBePicked()) {
				deck.cards.add(card);
				this.cardsAvailable.remove(card);
			}
		}

		for (Rune rune : runes) {
			if (rune.getMustBePicked()) {
				deck.runes.add(rune);
				this.runesAvailable.remove(rune);
			}
		}
	}

	public void transformDeck(Deck deck) throws CloneNotSupportedException {
        //logger.info("Transforming deck");
        List<Card> cards = new ArrayList<>(deck.cards);
        for (Card card : cards) {
			if (card.getDescription().contains("(")) {
                Card cardCloned = (Card) card.clone();
                String cardBase = cardCloned.getDescription().replace("(", "").replace(")", "");
                Integer optionsNumber = cardBase.split("\\|").length;
                Random random = new Random();
                String pickedOption = cardBase.split("\\|")[random.nextInt(optionsNumber)].trim();
                cardCloned.setDescription(pickedOption);
                //logger.info(cardCloned.getDescription());
                deck.cards.remove(card);
                deck.cards.add(cardCloned);
            }
		}
	}

	public Integer pickBestDeckGen(Deck deck, Integer avgNumber) {

		Deck bestDeck = new Deck();
		Integer numCards = 0;

		List<Deck> processedDecks = new ArrayList<Deck>(decksProcessed);
		Integer decksToProc = Math.min(5, decksProcessed.size());
		Collections.sort((List<Deck>) processedDecks, Collections.reverseOrder());
		// collect all cards and runes from top decks
		for (int i = 0; i < decksToProc; i++) {
			Deck pickedDeck = processedDecks.get(i);
			numCards += pickedDeck.cards.size();
			Collection<Card> pickedCards = pickedDeck.cards;
			Collection<Card> bestCards = bestDeck.cards;
			Collection<Rune> pickedRunes = pickedDeck.runes;
			Collection<Rune> bestRunes = bestDeck.runes;
			if (bestDeck.cards.size() > 0) {
				Collection<Card> cardsIntersect = CollectionUtils.intersection(pickedCards, bestCards);
				Collection<Rune> runesIntersect = CollectionUtils.intersection(pickedRunes, bestRunes);
				bestDeck.cards.clear();
				bestDeck.cards.addAll(cardsIntersect);
				bestDeck.runes.clear();
				bestDeck.runes.addAll(runesIntersect);
			} else {
				bestDeck.cards.clear();
				bestDeck.cards.addAll(pickedDeck.cards);
				bestDeck.runes.clear();
				bestDeck.runes.addAll(pickedDeck.runes);
			}

		}
		for (Card pickedCard : bestDeck.cards) {
			if (this.cardsAvailable.contains(pickedCard)) {
				deck.cards.add(pickedCard);
				this.cardsAvailable.remove(pickedCard);
			}
		}
		for (Rune pickedRune : bestDeck.runes) {
			if (this.runesAvailable.contains(pickedRune)) {
				deck.runes.add(pickedRune);
				this.runesAvailable.remove(pickedRune);
			}
		}
		
		Random random = new Random();
		if (random.nextInt(100) < 5) {
			mutateDeck(deck);
		}
		
		logger.info("Best intersect " + deck.cards);
		logger.info("Best intersect " + deck.runes);

		return numCards / decksToProc;
	}

	public void mutateDeck(Deck deck) {
		logger.info("Mutating");
        	//Util.addToLog(this.textAreaLog, "Mutating deck");
		Random random = new Random();
		Integer cardsToMutate = Math.max(1, random.nextInt(3));
		cardsToMutate = Math.min(cardsToMutate, deck.cards.size());
		for (int i = 0; i < cardsToMutate; i++) {
			Card pickedCard = deck.cards.get(random.nextInt(deck.cards.size()));
			deck.cards.remove(pickedCard);
			cardsAvailable.add(pickedCard);
		}

		Integer runesToMutate = Math.max(1, random.nextInt(2));
		runesToMutate = Math.min(runesToMutate, deck.runes.size());
		for (int i = 0; i < runesToMutate; i++) {
			Rune pickedRune = deck.runes.get(random.nextInt(deck.runes.size()));
			deck.runes.remove(pickedRune);
			runesAvailable.add(pickedRune);
		}

	}

	// public Integer pickBestDeck(Deck deck, Integer avgNumber) {
	// Deck tempDeck = new Deck();
	// Deck bestDeck = new Deck();
	// Random rand = new Random();
	// Integer numCards = 0;
	//
	// List<Deck> processedDecks = new ArrayList<Deck>(decksProcessed);
	// Integer decksToProc = Math.min(5, decksProcessed.size());
	// Collections.sort((List<Deck>) processedDecks,
	// Collections.reverseOrder());
	// // collect all cards and runes from top decks
	// for (int i = 0; i < decksToProc; i++) {
	// Deck pickedDeck = processedDecks.get(i);
	// // logger.info("processing best deck at mpm " + pickedDeck.mpm);
	// tempDeck.cards.addAll(pickedDeck.cards);
	// tempDeck.runes.addAll(pickedDeck.runes);
	// }
	// // get card frequency map
	// Map<Card, Double> cardFrequency = new TreeMap<>();
	// Set<Card> cardNames = new HashSet<>();
	// cardNames.addAll(tempDeck.cards);
	// for (Card card : cardNames) {
	// cardFrequency.put(card, (double) Collections.frequency(tempDeck.cards,
	// card));
	// }
	//
	// // get rune frequency map
	// Map<Rune, Double> runeFrequency = new TreeMap<>();
	// Set<Rune> runeNames = new HashSet<>();
	// runeNames.addAll(tempDeck.runes);
	// for (Rune rune : runeNames) {
	// runeFrequency.put(rune, (double) Collections.frequency(tempDeck.runes,
	// rune));
	// }
	//
	// cardFrequency = sortByValue(cardFrequency);
	// runeFrequency = sortByValue(runeFrequency);
	//
	// // logger.info(cardFrequency);
	// // logger.info(runeFrequency);
	//
	// // get best cards
	// Integer cardsToPick = rand.nextInt(Integer.min(cardFrequency.size(),
	// avgNumber));
	// // logger.info(cardsToPick);
	// Integer runesToPick = rand.nextInt(Integer.min(runeFrequency.size(), 4));
	//
	// for (int i = 0; i < cardsToPick; i++) {
	// if (checkCost) {
	// Integer innerIter = 1;
	// while (innerIter < cardsAvailable.size()) {
	// Card pickedCard = (Card) cardFrequency.keySet().toArray()[i];
	// if (pickedCard.getCost() + deck.cost <= this.maxCost) {
	// deck.cards.add(pickedCard);
	// cardsAvailable.remove(pickedCard);
	// deck.cost += pickedCard.getCost();
	// innerIter = cardsAvailable.size() + 1;
	// // logger.info(deck.cost);
	// } else {
	// cardsAvailable.remove(pickedCard);
	// innerIter++;
	// }
	// }
	// } else {
	// Card pickedCard = (Card) cardFrequency.keySet().toArray()[i];
	// deck.cards.add(pickedCard);
	// cardsAvailable.remove(pickedCard);
	// }
	// }
	//
	// for (int i = 0; i < runesToPick; i++) {
	// deck.runes.add((Rune) runeFrequency.keySet().toArray()[i]);
	// runesAvailable.remove(runeFrequency.keySet().toArray()[i]);
	// }
	//
	// // logger.info(deck.cards);
	// // logger.info(deck.runes);
	// //
	// // logger.info(numCards);
	// // logger.info(decksToProc);
	// // logger.info(numCards / decksToProc);
	// return tempDeck.cards.size() / decksToProc;
	// }

	public void pickDeck(String mode, String minCards, String waitToPlay) throws IOException, CloneNotSupportedException {
		// logger.info("picking deck");

		Random rand = new Random();
		Boolean newDeck = false;
		Integer iterations = 0;

		while (!newDeck) {
			newDeck = false;
			Deck deck = new Deck();
			reset();
			Integer cardsToPick = 0;
			Integer runesToPick = 0;
			Integer avgCards = 5;

			if (mode.equals("best")) {
				if (decksProcessed.size() > 0) {
					pickMustCards(deck);
					avgCards = pickBestDeckGen(deck, avgCards);
//
					if (rand.nextInt(100) < 25 && iterations > 250 && deck.cards.size() > 0) {
						logger.info("Mutating");
						mutateDeck(deck);
					}
//
//					// logger.info(avgCards);
//					List<Integer> maxSizes = new ArrayList<Integer>();
//					maxSizes.add(10);
//					maxSizes.add(avgCards + rand.nextInt(3));
//					List<Integer> minSizes = new ArrayList<Integer>();
//					minSizes.add(Integer.valueOf(minCards));
//					minSizes.add(avgCards - rand.nextInt(3));
//					Collections.sort(maxSizes);
//					Collections.sort(minSizes, Collections.reverseOrder());
//					Integer maxCardsToPick = maxSizes.get(0);
//					Integer minCardsToPick = minSizes.get(0);
//					// logger.info(deck.cards.size());
//					// logger.info(maxCardsToPick);
//					// logger.info(minCardsToPick);
//					Integer cardSpan = maxCardsToPick - minCardsToPick;
//					Integer desiredCardNumber = minCardsToPick + cardSpan;
//					cardsToPick = desiredCardNumber - deck.cards.size();
//					// logger.info(cardsToPick);
//					runesToPick = 4 - deck.runes.size();
				} else {
					cardsToPick = 10;
					runesToPick = 4;
				}
			} else if (mode.equals("runes")) {
				cardsToPick = 0;
				runesToPick = 4;
				deck.cards.addAll(bestDeck.cards);
			} else if (mode.equals("random")) {
				pickMustCards(deck);
			}

//			if (!target.equals("demon")) {
			cardsToPick = Math.min(cardsAvailable.size(),10 - deck.cards.size());
			runesToPick = Math.min(runesAvailable.size(), 4 - deck.runes.size());
//			}

			// if (cardsToPick < Integer.valueOf(minCards)) {
			// cardsToPick = Integer.valueOf(minCards);
			// }

			// logger.info(decksProcessed);
			// logger.info("picking other one");
			// get max cards to pick
			// Integer maxCardsToPick = Math.min(cardsAvailable.size(), 10 -
			// deck.cards.size());
			// min deck size
			// Integer minDeckBoundary = Math.max(Math.max(minCards, avgCards -
			// 1), deck.cards.size());
			// logger.info(minDeckBoundary + " minds");
			// logger.info(deck.cards.size() + " cnow");
			// logger.info(maxCardsToPick + " mctp");
			// logger.info(avgCards + " avg");
			// Integer cardsToPick = minDeckBoundary - deck.cards.size() +
			// rand.nextInt(10 - minDeckBoundary) + 1;
			// cardsToPick = Math.min(maxCardsToPick, cardsToPick);
			// if (deck.cards.size() >= minDeckSize) {
			// logger.info("first");
			// cardsToPick = rand.nextInt(maxCardsToPick);
			// } else {
			// logger.info("second");
			// cardsToPick = minDeckSize + rand.nextInt(10 - minDeckSize -
			// deck.cards.size());
			// }
			// logger.info(cardsToPick + " ctp");
			// logger.info(cardsToPick + deck.cards.size() + " ctotal");
			// logger.info("cards to pick:" + cardsToPick.toString());

			for (Card card : deck.cards) {
				deck.cost += card.getCost();
			}
			logger.info("Picked card cost : " + deck.cost);

			for (int i = 0; i < cardsToPick; i++) {
				// logger.info(cardsAvailable.size());
				// logger.info("rand" +
				// rand.nextInt(cardsAvailable.size()));
				if (checkCost) {
					Integer innerIter = 1;

					while (innerIter < cardsAvailable.size()) {
						Card pickedCard = cardsAvailable.get(rand.nextInt(cardsAvailable.size()));
						if (pickedCard.getCost() + deck.cost <= this.maxCost) {
							deck.cards.add(pickedCard);
							cardsAvailable.remove(pickedCard);
							deck.cost += pickedCard.getCost();
							innerIter = cardsAvailable.size() + 1;
							// logger.info(deck.cost);
						} else {
							cardsAvailable.remove(pickedCard);
							innerIter++;
						}
					}
				} else {
					Card pickedCard = cardsAvailable.get(rand.nextInt(cardsAvailable.size()));
					deck.cards.add(pickedCard);
					cardsAvailable.remove(pickedCard);
				}
			}
			// Integer runesToPick = 4 - deck.runes.size();
			if (target.equals("ew") && !mode.equals("random"))
				setCardsOrder(deck);

			for (int i = 0; i < runesToPick; i++) {
				// logger.info(runesInit.size());
				// logger.info(runesAvailable.size());
				Rune pickedRune = runesAvailable.get(rand.nextInt(runesAvailable.size()));
				deck.runes.add(pickedRune);
				runesAvailable.remove(pickedRune);
			}
			// deck.sort();
			transformDeck(deck);
			currentDeck = deck;
			// logger.info(deck.hashCode());
			// logger.info(deck.cards);
			// logger.info(deck.runes);
			// logger.info(decksProcessed);
			if (!decksProcessed.contains(deck)) {
				// logger.info("new one");
//				if (!target.equals("ew")) {
					deck.dumpToTxt(this.playerLvl, false, null);
//				} else {
//					deck.dumpToTxt(this.playerLvl, true, waitToPlay);
//				}
				decksProcessed.add(deck);
				newDeck = true;
				break;
			}
			iterations++;

			if ((!mode.equals("runes") && iterations > 1000) || (mode.equals("runes") && iterations > 2000)) {
				logger.info("need to break");
				deck.cards.clear();
				break;
			}
		}
	}

	public void run(String mode, TextArea textAreaLog) throws IOException {
		// logger.info ("Fire sim run");
        this.textAreaLog = textAreaLog;
		List<String> options = new ArrayList<String>();
		if (!this.javaPath.equals("")) {
			options.add(new File(this.javaPath, "java").toString());
		} else {
			options.add("java");
		}
		options.add("-jar");
		options.add("ek-battlesim.jar");
		options.add("-npb");
		Integer cores = Runtime.getRuntime().availableProcessors() * 2 * 3;
		// options.add("-th");
		// options.add(cores.toString());
		if (!mode.equals("confirm")) {
			options.add("-i");
			options.add(this.innerIterations);
		} else {
                    if(!target.equals("demon")) {
			options.add("-i");
			Integer iterationsConfirm = Integer.valueOf(this.innerIterations) * 3;
			options.add(iterationsConfirm.toString());
                    }
                        
		}
		options.add("-dk");
		options.add(App.tmpDeckFile);
		if (target.equals("demon")) {
			options.add("-dm");
			options.add(this.targetValue);
		} else if (target.equals("ew")) {
			options.add("-ew");
			options.add(this.targetValue);
		} else if (target.equals("kw")) {
			options.add("-kw");
		} else if (target.equals("hydra")) {
			options.add("-hy");
			options.add("\"h5\"");
		} else if (target.equals("thief")) {
			options.add("-thief");
		} else if (target.equals("arena")) {
			options.add("-vs");
			options.add(App.arenaDir);
		}
		ProcessBuilder processBuilder = new ProcessBuilder(options);
		processBuilder.directory(new File(App.dataDir, App.binDir));
		Process simProcess = processBuilder.start();
		String simOutput = getOutputValue(simProcess.getInputStream(), target);
		// logger.info(simOutput);
		if (currentDeck.cards.size() == 0) {
			Util.dumpToTxt(simOutput, App.tmpOutputFile);
			System.exit(0);
		}
//		logger.info("Current :  " + currentDeck.mpm);
//        logger.info("Top:  " + topMpm);
		if (currentDeck.mpm > topMpm || (currentDeck.mpm / topMpm > 0.95 && !target.equals("hydra")) ||
                (topMpm / currentDeck.mpm > 0.95 && target.equals("hydra"))) {
			if (!mode.equals("confirm")) {
				logger.info("Need to confirm");
				this.run("confirm", this.textAreaLog);
			}
			if (currentDeck.mpm > topMpm) {
				Util.dumpToTxt(simOutput, App.tmpOutputFile);
				this.bestDeck.output.set(simOutput);
				this.bestDeck.cards.clear();
				this.bestDeck.cards.addAll(currentDeck.cards);
				this.bestDeck.runes.addAll(currentDeck.runes);
				this.topMpm = currentDeck.mpm;
				logger.info("we've got top at : " + currentDeck.mpm);
			}
		}
		simProcess.destroy();
	}

	private String getOutputValue(InputStream inputStream, String target) throws IOException {
		StringBuilder sb = new StringBuilder();
		BufferedReader br = null;
		Boolean foundResult = false;
		// logger.info("Getting best val");
		// logger.info(this.target);
		String valueToLookFor = "";
		if (target.equals("demon")) {
			valueToLookFor = ".*MPM.*";
		} else if (target.equals("ew")) {
			valueToLookFor = ".*\\(DPF\\).*";
		} else if (target.equals("kw")) {
			valueToLookFor = ".*Average wins per loss.*";
		} else if (target.equals("hydra")) {
			valueToLookFor = ".*Global.*";
		} else if (target.equals("thief")) {
			valueToLookFor = ".*Global.*";
		} else if (target.equals("arena")) {
			valueToLookFor = ".*Global.*";
		}
		try {
			br = new BufferedReader(new InputStreamReader(inputStream));
			String line = null;
			// logger.info(valueToLookFor);
			while ((line = br.readLine()) != null) {
				if (line.matches(valueToLookFor) & !foundResult) {
					foundResult = true;
					// logger.info(line);
					if (this.target.equals("demon")) {
						// logger.info("trying to get mpm");
						currentDeck.mpm = Double.valueOf(line.split("\\|")[1].trim());
						// logger.info(currentDeck.mpm);
					} else if (this.target.equals("ew")) {
						// logger.info("trying to get mpf");
						// logger.info(line);
						currentDeck.mpm = (Double.valueOf(line.split("\\|")[1].split("/")[0].trim()));
						// logger.info(currentDeck.mpm);
					} else if (this.target.equals("kw")) {
						// logger.info("trying to get avg per life");
						currentDeck.mpm = (Double.valueOf(line.split("\\|")[1].trim()));
						// logger.info(currentDeck.mpm);
					} else if (this.target.equals("arena")) {
						// logger.info("trying to get avg per life");
						// logger.info(line);
						currentDeck.mpm = (Double.valueOf(line.split("\\|")[1].trim().replace("%", "")));
						// logger.info(currentDeck.mpm);
					} else if (this.target.equals("hydra") || this.target.equals("thief")) {
						// logger.info("trying to get avg rounds to kill " +
						// this.target);
						currentDeck.mpm = -(Double.valueOf(line.split("\\|")[1].trim().replace("+", "")));

						// Double percentage = 0.0;
						// Integer size = 0;
						// // logger.info(line);
						// String[] percValArr = line.split("\\|");
						// for (String percVal : percValArr) {
						// // logger.info(percVal);
						// if (percVal.contains("%")) {
						// percentage += Double.valueOf(percVal.replace("%",
						// "").trim());
						// size++;
						// }
						// }
						// currentDeck.mpm = percentage / size;
						// // logger.info(currentDeck.mpm);
					}

				}
				sb.append(line + System.getProperty("line.separator"));
			}
		} finally {
			logger.info("Current " + currentDeck.mpm);
			if (currentDeck.mpm == 0.0) {
				logger.info(currentDeck.cards);
				logger.info(currentDeck.runes);
			}
			br.close();
		}

		return sb.toString();
	}

	public static Map sortByValue(Map unsortedMap) {
		Map sortedMap = new TreeMap(new ValueComparator(unsortedMap));
		sortedMap.putAll(unsortedMap);
		return sortedMap;
	}

}
