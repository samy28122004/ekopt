package me.pogorzelski.ekopt;

public class Card implements Comparable<Card>, Cloneable {
	private String description;
	private Integer cost;
	private Boolean mustBePicked;
	private Integer cardId;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getCost() {
		return cost;
	}

	public void setCost(Integer cost) {
		this.cost = cost;
	}

	public Boolean getMustBePicked() {
		return mustBePicked;
	}

	public void setMustBePicked(Boolean mustBePicked) {
		this.mustBePicked = mustBePicked;
	}

	@Override
	public int compareTo(Card o) {
		// TODO Auto-generated method stub
		return this.cardId.compareTo(o.cardId);
	}

	@Override
	public int hashCode() {
		int resultId = cardId.hashCode();
		int resultDesc = description.hashCode();
		int result = 31 * resultId + resultDesc;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null || getClass() != obj.getClass())
			return false;

		Card card = (Card) obj;

		if (cardId.equals(card.getCardId()) && description.equals(card.getDescription()))
			return true;

		//
		// if (description.equals(card.description))
		// return true;

		return false;

	}

	protected Object clone() throws CloneNotSupportedException {
		Card cloned = (Card)super.clone();
		return cloned;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return description;
	}

	public Integer getCardId() {
		return cardId;
	}

	public void setCardId(Integer cardId) {
		this.cardId = cardId;
	}

}
